<?php

/**
 *
 * name: _get_user_todo_ly_values
 * @param
 *  $uid  this is the user uid
 * @return
 *  if there is information give back the values
 *  If there isnot information give back FALSE
 */
function _get_user_todo_ly_encodevalues($uid = NULL) {
  if($uid == NULL) {
    Global $user;
    $uid = $user->uid;    
  }

  //declare variables
  $email= '';
  $password = '';

  //get todo.ly account information
  $email = variable_get('todoly-email-'.$uid,'');
  $password = variable_get('todoly-password-'.$uid,'');

  //validate that there is a email and a password information.
  //this don't check if the information is correct, only to kknow that there is something.'
  if((!empty($email)) && (!empty($password))) {

    $string = $email.':'.$password;
    $baseencodestring = base64_encode($string);
    return $baseencodestring;
  }
  return FALSE;
}

/**
 * This and the rest of functions get the values from the drupal_httprequest function
 * http://api.drupal.org/api/drupal/includes--common.inc/function/drupal_http_request/7
 *
 * name: _todo_ly_conection
 * @param
 *  $uid:  user uid to get the values
 * @return
 *  boolean
 */

function _todo_ly_conection($uid) {



}
