<?php
/*
 *      todo_ly.examples.inc
 *      
 *      Copyright 2012 Oskar Calvo <oskar@labo>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

 /**
  * Este archivo únicamente es un conjunto de ejemplos de código para 
  * entender como funciona la API de 
  * 
  * 
  */

/**
 *
 * Example1
 * verificamos que nos conectamos correctamente
 * Api: http://todo.ly/apiwiki/?authentication/basic-authentication/todo-ly-rest-api-method-get-authenticationisauthenticated
 * Esta función únicamente nos ayuda a comprobar que los datos (correoelectrónico:contraseña) en base 64 son correctos, 
 * y nos autentificamos con ellos.
 *
 */

$options = array();
$options['headers']['Authorization'] = _get_user_todo_ly_encodevalues();
$options['method'] = 'GET';
$url  ='https://todo.ly/api/authentication/isauthenticated.xml';
$result = drupal_http_request($url,$options);

dpm($result);

/**
 *
 * Example2
 * Generamos un token
 * En esta función necesitamos la autentificación de headers para lograr el token que nos permitirá posteriormente 
 * comunicarnos con la herramienta.
 *
 */
Global $user;
$options = array();
$options['headers']['Authorization'] = _get_user_todo_ly_encodevalues();
$options['method'] = 'GET';
$url = 'https://todo.ly/api/authentication/token.xml/';

$result = drupal_http_request($url,$options);
dpm($result);

//parsear el xml
//pasamos todo el valor de data a la variable $token
$token = $result->data;
//cargamos con simplexml la cada de xml y lo convertimos en un array
$xml = (array) simplexml_load_string($token);

dpm($xml['TokenString']);
variable_set('todo-ly-'.$user->uid,$xml['TokenString']);

/**
 *
 * Example3
 * Creamos una tarea nueva
 * Es necesario usar el token que se ha debido generar previamente.
 * Es necesario como mínimo facilitar el valor de Content (vendría a ser el title).
 */
Global $user;
$options = array();
$options['headers']['token'] = variable_get('todo-ly-'.$user->uid,'');
$options['data'] = '
  <ItemObject>    
    <Content>Generación de tarea</Content>    
  </ItemObject>';
$options['method'] = 'POST';
$url = 'https://todo.ly/api/items.xml';
$result = drupal_http_request($url,$options);
print_r ($result);


/**
 *
 * Example4
 * Actualizamos una tarea existente
 * Para actualizar una tarea es necesario por un lado facilitar el token.
 * En data hay que facilitar los datos que se quieren actualizar.
 * IMPORTANTE: dispondemos del campo de fecha:
 * <LastUpdatedDate>2012-01-01T23:04:13.267</LastUpdatedDate> para controlar cuando se han actualizado 
 * las tareas.
 * Este campo sería fundamental para tener una sincronización entre Drupal y todo_ly. Sobre todo a la hora de 
 * analizar como se sincronizarían.
 * En la url hay que facilitar el id de la tarea que se quiere actualizar.
 *
 */
$options = array();
$options['headers']['token'] = '891681f012514dc69a0577ce9f773434';
$options['data'] = '
  <ItemObject>    
     <Checked>true</Checked> 
     <Content>Generación de tarea2</Content>     
  </ItemObject>';
$options['method'] = 'POST';
$url = 'https://todo.ly/api/items/2869084.xml';
$result = drupal_http_request($url,$options);
print_r ($result);




/**
 *
 * Example 5
 * Cargamos una tarea existente
 * Para cargar una tarea hay que facilitar dos elementos.
 * El token craedo previamente y el id de la tarea en la url.
 *
 */

$options = array();
$options['headers']['token'] = '891681f012514dc69a0577ce9f773434';
$options['method'] = 'GET';
$url = 'https://todo.ly/api/items/2869084.xml';
$result = drupal_http_request($url,$options);
print_r ($result);



/**
 *
 * Example 6
 * Creamos un usuario
 *
 */
$options = array();
$options['method'] = 'POST';
$options['data'] = '
  <UserObject>
  	<Email>user@email.com</Email>
  	<FullName>Joe Blow</FullName>
  	<Password>pASswoRd</Password>
  </UserObject>';
$url = 'https://todo.ly/api/user.xml';
$result = drupal_http_request($url,$options);
print_r ($result);

/**
 *
 * Example 7.1
 * Actualizar los datos de un usuario
 * Para actualizar los datos de un usuario utilizaremos el método PUT, también podemos con POST.
 * Únicamente se actualizan aquellos datos que le pasemos por data.
 *
 */

$options = array();
$options['headers']['Authorization'] = _get_user_todo_ly_encodevalues();
$options['method'] = 'PUT';
$options['data'] = '
  <UserObject>
  	<Email>user@email.com</Email>
  	<FullName>Joe Blow</FullName>
  	<Password>pASswoRd</Password>
  </UserObject>';
$url = 'https://todo.ly/api/user/0.xml';
$result = drupal_http_request($url,$options);
print_r ($result);

/**
 *
 * Example 7.2
 * Cargamos usuarios por encodestring 
 *
 */

$options = array();
$options['headers']['Authorization'] = _get_user_todo_ly_encodevalues();
$options['method'] = 'GET';
$url = 'https://todo.ly/api/user.xml';
$result = drupal_http_request($url,$options);
print_r ($result);



?>
