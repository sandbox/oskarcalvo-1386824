<?php

/**
 *
 * Form setting to get the values from todo.ly account
 *
 *
 */
function todo_ly_account_form(){
  Global $user;
  $form = array();

  $form['submit'] = array(
    '#type' => 'item',
    '#title' => t('Fill all the elements to conect with todo.ly webservice'),
  );


  $form['todoly-email-'.$user->uid] = array(
    '#type' => 'textfield',
    '#title' => t('Todo.ly\'s account mail'),
    '#maxlength' => 64,
    '#size' => 15,
    '#element_validate' => array('_todo_ly_email_validate'),
    '#default_value' => variable_get('todo_ly-mail-'.$user->uid, ''),
  );

  $form['todoly-password-'.$user->uid] = array(
    '#type' => 'password',
    '#title' => t('Todo.ly\'s account password'),
    '#maxlength' => 64,
    '#size' => 15,
    '#default_value' => variable_get('todo_ly-password-'.$user->uid,''),
  );

  return system_settings_form($form);
}

/**
 *
 * validate email field
 * @name
 *  _todo_ly_email_validate
 * @param
 *  $element: the element to validate
 *  $form_state: the form_state from the form
 * @return
 *  If the element is not an email give back an error.
 *
 */
function _todo_ly_email_validate($element, &$form_state) {
  if(!valid_email_address($element['#value'])){
    form_error($element, t('Seems that the email is wrong'));
  }
}
